/**
 * Copyright 2015 Eric Müller <mekose@protonmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package common.testing.reporter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import common.testing.Scenario;
import common.testing.Scenarios;
import common.testing.Step;

/**
 * @author Eric Müller <mekose@protonmail.com>
 * @date 23.01.2015 07:07:19
 * @file TextScenarioReporter.java
 */
public class TextScenarioReporter extends TestWatcher implements IScenarioReporter {

	/**
	 * The out.
	 */
	private PrintStream out = null;

	/**
	 * Constructor
	 */
	public TextScenarioReporter() {
		this(System.out);
	}

	/**
	 * Constructor
	 * @param reportFile The report file to use.
	 */
	public TextScenarioReporter(final String reportFile) {
		this(new File(reportFile));
	}

	/**
	 * Constructor
	 * @param reportFile The report file to use.
	 */
	public TextScenarioReporter(final File reportFile) {
		try {
			out = new PrintStream(new FileOutputStream(reportFile));
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Constructor
	 * @param out The print stream to use.
	 */
	public TextScenarioReporter(final OutputStream out) {
		if (out instanceof PrintStream) {
			this.out = (PrintStream) out;
		} else {
			this.out = new PrintStream(out);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.junit.rules.TestWatcher#starting(org.junit.runner.Description)
	 */
	@Override
	protected void starting(final Description description) {
		// Clear the scenarios after every test method call.
		final List<Scenario> scenarios = Scenarios.scenarios();
		if (scenarios != null) {
			scenarios.clear();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.junit.rules.TestWatcher#failed(java.lang.Throwable, org.junit.runner.Description)
	 */
	@Override
	protected void failed(final Throwable e, final Description description) {
		final StringBuilder builder = new StringBuilder();
		render(e, description, builder);
		out.println(builder.toString());
	}

	/*
	 * (non-Javadoc)
	 * @see org.junit.rules.TestWatcher#succeeded(org.junit.runner.Description)
	 */
	@Override
	protected void succeeded(final Description description) {
		final StringBuilder builder = new StringBuilder();
		render(null, description, builder);
		out.println(builder.toString());
	}

	/**
	 * Renders a test method call.
	 *
	 * @param t The throwable (optional).
	 * @param description The description.
	 * @param builder The builder.
	 */
	private void render(final Throwable t, final Description description, final StringBuilder builder) {
		// The test description.
		final String s = String.format("Test: %s#%s()", description.getClassName(), description.getMethodName());
		builder.append(s).append("\n");
		// A simple dividing line with the length of the test description above.
		final String line = String.format("%0" + s.length() + "d", 0).replace("0", "=") + "\n";
		builder.append(line);
		// The rendered scenarios of the current test method.
		renderScenarios(builder);
		// The same line as above.
		builder.append(line);
		// If the test failed we got an exception otherwise not.
		if (t != null) {
			builder.append("  => Test failed!\n\n");
			final StringWriter writer = new StringWriter();
			t.printStackTrace(new PrintWriter(writer));
			builder.append(writer);
		} else {
			builder.append("  => Test succeeded!\n");
		}
	}

	/**
	 * Renders the currently exsiting scenarios.
	 *
	 * @param builder The builder.
	 */
	private void renderScenarios(final StringBuilder builder) {
		final List<Scenario> scenarios = Scenarios.scenarios();
		for (final Scenario scenario : scenarios) {
			final String description = scenario.getDescription();
			builder.append("Scenario: ");
			if (description == null) {
				builder.append("<unknown>");
			} else {
				builder.append(description);
			}
			builder.append("\n");
			final List<Step> steps = scenario.getSteps();
			if (steps != null) {
				for (final Step step : steps) {
					builder.append("  => ").append(String.format("%8s", "[" + step.getType().name() + "] ")).append(step.getMessage()).append("\n");
				}
			} else {
				builder.append("No steps defined yet!");
			}
		}
	}
}
