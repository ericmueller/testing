/**
 * Copyright 2015 Eric Müller <mekose@protonmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package common.testing.reporter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.List;

import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import common.testing.Scenario;
import common.testing.Scenarios;
import common.testing.Step;

/**
 * @author Eric Müller <mekose@protonmail.com>
 * @date 23.01.2015 07:07:19
 * @file TextScenarioReporter.java
 */
public class XmlScenarioReporter extends TestWatcher implements IScenarioReporter {

	/**
	 * The out.
	 */
	private PrintStream out = null;

	private int totalNumberOfTestMethods = -1;
	private int currentTest = 0;

	/**
	 * Constructor
	 */
	public XmlScenarioReporter() {
		this(System.out);
	}

	/**
	 * Constructor
	 * @param reportFile The report file to use.
	 */
	public XmlScenarioReporter(final String reportFile) {
		this(new File(reportFile));
	}

	/**
	 * Constructor
	 * @param reportFile The report file to use.
	 */
	public XmlScenarioReporter(final File reportFile) {
		try {
			out = new PrintStream(new FileOutputStream(reportFile));
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Constructor
	 * @param out The print stream to use.
	 */
	public XmlScenarioReporter(final OutputStream out) {
		if (out instanceof PrintStream) {
			this.out = (PrintStream) out;
		} else {
			this.out = new PrintStream(out);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.junit.rules.TestWatcher#starting(org.junit.runner.Description)
	 */
	@Override
	protected void starting(final Description description) {

		++currentTest;
		
		if (totalNumberOfTestMethods == -1) {
			totalNumberOfTestMethods = 0;
			try {
				final Class<?> clazz = Class.forName(description.getClassName());
				final Method[] methods = clazz.getMethods();
				for (final Method method : methods) {
					if (method.isAnnotationPresent(Test.class)) {
						++totalNumberOfTestMethods;
					}
				}
			} catch (final Exception e) {
				throw new RuntimeException(e);
			}
		}

		// Clear the scenarios after every test method call.
		final List<Scenario> scenarios = Scenarios.scenarios();
		if (scenarios != null) {
			scenarios.clear();
		}

		if (currentTest == 1) {
			final StringBuilder builder = new StringBuilder();
			builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			builder.append("<test-report>");
			builder.append("<test-class>").append(description.getClassName()).append("</test-class>");
			out.println(builder.toString());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.junit.rules.TestWatcher#finished(org.junit.runner.Description)
	 */
	@Override
	protected void finished(final Description description) {

		if (currentTest == totalNumberOfTestMethods) {
			final StringBuilder builder = new StringBuilder();
			builder.append("</test-report>");
			out.println(builder.toString());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.junit.rules.TestWatcher#failed(java.lang.Throwable, org.junit.runner.Description)
	 */
	@Override
	protected void failed(final Throwable e, final Description description) {
		final StringBuilder builder = new StringBuilder();
		render(e, description, builder);
		out.println(builder.toString());
	}

	/*
	 * (non-Javadoc)
	 * @see org.junit.rules.TestWatcher#succeeded(org.junit.runner.Description)
	 */
	@Override
	protected void succeeded(final Description description) {
		final StringBuilder builder = new StringBuilder();
		render(null, description, builder);
		out.println(builder.toString());
	}

	/**
	 * Renders a test method call.
	 *
	 * @param t The throwable (optional).
	 * @param description The description.
	 * @param builder The builder.
	 */
	private void render(final Throwable t, final Description description, final StringBuilder builder) {

		//		builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		//		builder.append("<test-report>");
		//		builder.append("<test-class>").append(description.getClassName()).append("</test-class>");

		//		Test: de.torfmolch.fop.document.XsltDocumentTest#testCreateDocument_WithoutCreatingStylesheetElement()
		//		======================================================================================================
		//		Scenario: Test create a XSLT document without creating a xs:stylesheet element
		//		  => [GIVEN] A XSLT document object
		//		  =>  [WHEN] Not creating a xs:stylesheet element
		//		  =>  [THEN] Result should be only a XML declaration
		//		======================================================================================================
		//		  => Test failed!

		//		<?xml version="1.0" encoding="UTF-8"?>
		//		<test-report>
		//			<test-class>de.torfmolch.fop.document.XsltDocumentTest</test-class>
		//				<test name="testCreateDocument_WithoutCreatingStylesheetElement">

		// The test description.
		builder.append("<test name=\"").append(description.getMethodName()).append("\">");

		renderScenarios(builder);

		// If the test failed we got an exception otherwise not.
		if (t != null) {
			builder.append("<test-result>FAILURE</test-result>");
			builder.append("<exception><![CDATA[");
			final StringWriter writer = new StringWriter();
			t.printStackTrace(new PrintWriter(writer));
			builder.append(writer);
			builder.append("]]>");
		} else {
			builder.append("<result>SUCCESS</result>");
		}

		builder.append("</test>");
	}

	/**
	 * Renders the currently exsiting scenarios.
	 *
	 * @param builder The builder.
	 */
	private void renderScenarios(final StringBuilder builder) {

		final List<Scenario> scenarios = Scenarios.scenarios();

		if (scenarios == null || scenarios.size() < 1) {
			builder.append("<scenarios />");
			return;
		}

		builder.append("<scenarios>");

		for (final Scenario scenario : scenarios) {

			builder.append("<scenario>");

			final String description = scenario.getDescription();
			if (description == null) {
				builder.append("<description />");
			} else {
				builder.append("<description><![CDATA[").append(description).append("]]></description>");
			}

			builder.append("\n");
			final List<Step> steps = scenario.getSteps();
			if (steps != null) {
				builder.append("<steps>");
				for (final Step step : steps) {
					builder.append("<" + step.getType().name().toLowerCase() + ">");
					builder.append("<![CDATA[").append(step.getMessage()).append("]]>");
					builder.append("</" + step.getType().name().toLowerCase() + ">");
				}
				builder.append("</steps>");
			} else {
				builder.append("<steps />");
			}

			builder.append("</scenario>");
		}

		builder.append("</scenarios>");
	}
}
