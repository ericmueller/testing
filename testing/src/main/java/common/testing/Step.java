/**
 * Copyright 2013 Eric Müller <mekose@protonmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package common.testing;

/**
 * @author Eric Müller <mekose@protonmail.com>
 * @date 23.01.2015 06:44:05
 * @file Step.java
 */
public final class Step {

	/**
	 * The types.
	 */
	public static enum Type {

		GIVEN, WHEN, THEN;
	}

	/**
	 * The type.
	 */
	private Type type = null;

	/**
	 * The message.
	 */
	private String message = null;

	/**
	 * Constructor
	 */
	public Step() {
	}

	/**
	 * Constructor
	 * @param type The step type.
	 * @param message The message.
	 */
	public Step(final Type type, final String message) {
		setType(type);
		setMessage(message);
	}

	/**
	 * @return Returns the type.
	 */
	public Type getType() {
		return type;
	}

	/**
	 * @param type The type to set.
	 */
	public void setType(final Type type) {
		this.type = type;
	}

	/**
	 * @return Returns the message.
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message The message to set.
	 */
	public void setMessage(final String message) {
		this.message = message;
	}
}
