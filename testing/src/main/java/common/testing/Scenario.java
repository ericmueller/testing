/**
 * Copyright 2015 Eric Müller <mekose@protonmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package common.testing;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Eric Müller <mekose@protonmail.com>
 * @date 23.01.2015 09:10:18
 * @file Scenario.java
 */
public class Scenario {

	/**
	 * The description.
	 */
	private String description = null;

	/**
	 * The steps.
	 */
	private List<Step> steps = null;

	/**
	 * Constructor
	 */
	public Scenario() {
		this(null);
	}

	/**
	 * Constructor
	 * @param description The scenarios description.
	 */
	public Scenario(final String description) {
		setDescription(description);
		steps = new ArrayList<Step>();
	}

	/**
	 * @param message The message of the step.
	 * @return Returns the scenario for method chaining.
	 */
	public Scenario given(final String message) {
		steps.add(new Step(Step.Type.GIVEN, message));
		return this;
	}

	/**
	 * @param message The message of the step.
	 * @return Returns the scenario for method chaining.
	 */
	public Scenario when(final String message) {
		steps.add(new Step(Step.Type.WHEN, message));
		return this;
	}

	/**
	 * @param message The message of the step.
	 * @return Returns the scenario for method chaining.
	 */
	public Scenario then(final String message) {
		steps.add(new Step(Step.Type.THEN, message));
		return this;
	}

	/**
	 * @return Returns the description.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description The description to set.
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * @return Returns the steps.
	 */
	public List<Step> getSteps() {
		return steps;
	}

	/**
	 * @param steps The steps to set.
	 */
	public void setSteps(final List<Step> steps) {
		this.steps = steps;
	}
}
