/**
 * Copyright 2015 Eric Müller <mekose@protonmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package common.testing;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import common.testing.reporter.IScenarioReporter;
import common.testing.reporter.TextScenarioReporter;
import common.testing.reporter.XmlScenarioReporter;

/**
 * This class is the central linchpin when working with scenarios in a test case.
 * It holds all scenarios that belongs to the test case. It also holds the reporter instance.
 * 
 * <p>Creating a new scenario</p>
 * <code>
 * Scenario scenario = Scenarios.newScenario("My scenario description");
 * </code>
 * 
 * <p>Describe the scenario</p>
 * <code>
 * scenario.given("A XSLT document object");
 * scenario.when("Not creating a xs:stylesheet element");
 * scenario.then("Result should be only a XML declaration");
 * </code>
 * 
 * @author Eric Müller <mekose@protonmail.com>
 * @date 23.01.2015 09:09:57
 * @file Scenarios.java
 */
public final class Scenarios {

	/**
	 * The thread local instance.
	 */
	private static ThreadLocal<Scenarios> threadLocal = new ThreadLocal<Scenarios>() {

		@Override
		protected Scenarios initialValue() {
			return new Scenarios();
		}
	};

	/**
	 * The scenarios.
	 */
	private List<Scenario> scenarios = null;

	/**
	 * The reporter.
	 */
	private IScenarioReporter reporter = null;

	/**
	 * Constructor
	 */
	private Scenarios() {
		scenarios = new ArrayList<Scenario>();
	}

	/**
	 * @return Returns the list of scenarios.
	 */
	public static List<Scenario> scenarios() {
		if (threadLocal.get() == null) {
			return null;
		}
		return threadLocal.get().scenarios;
	}

	/**
	 * @return Returns the new scenario.
	 */
	public static Scenario newScenario() {
		return newScenario(null);
	}

	/**
	 * Creates a reporter that reports to System.out.
	 * @return Returns the scenario reporter.
	 */
	public static IScenarioReporter reporter() {
		if (threadLocal.get().reporter != null) {
			return threadLocal.get().reporter;
		}
		threadLocal.get().reporter = new TextScenarioReporter();
		return threadLocal.get().reporter;
	}

	/**
	 * @param reportFile The report file to use.
	 * @return Returns the scenario reporter.
	 */
	public static IScenarioReporter reporter(final String reportFile) {
		if (threadLocal.get().reporter != null) {
			return threadLocal.get().reporter;
		}
		return reporter(new File(reportFile));
	}

	/**
	 * @param reportFile The report file to use.
	 * @return Returns the scenario reporter.
	 */
	public static IScenarioReporter reporter(final File reportFile) {
		if (threadLocal.get().reporter != null) {
			return threadLocal.get().reporter;
		}
		try {
			final String reportType = reportFile.getName().substring(reportFile.getName().lastIndexOf(".") + 1);
			return reporter(new FileOutputStream(reportFile), reportType);
		} catch (final FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * @param outputStream The output stream to use.
	 * @param reportType The type of the report (available at this time: txt or xml).
	 * @return Returns the scenario reporter.
	 */
	public static IScenarioReporter reporter(final OutputStream outputStream, final String reportType) {
		if (threadLocal.get().reporter != null) {
			return threadLocal.get().reporter;
		}
		if ("xml".equalsIgnoreCase(reportType)) {
			threadLocal.get().reporter = new XmlScenarioReporter(outputStream);
		} else if ("txt".equalsIgnoreCase(reportType)) {
			threadLocal.get().reporter = new TextScenarioReporter(outputStream);
		} else {
			throw new IllegalArgumentException("No report for report type: " + reportType + " available!");
		}
		return threadLocal.get().reporter;
	}

	/**
	 * @param description The scenarios description.
	 * @return Returns the new scenario.
	 */
	public static Scenario newScenario(final String description) {
		final Scenario scenario = new Scenario(description);
		threadLocal.get().scenarios.add(scenario);
		return scenario;
	}
}
