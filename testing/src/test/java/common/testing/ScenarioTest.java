/**
 * Copyright 2013 Eric Müller <mekose@protonmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package common.testing;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

import common.testing.reporter.IScenarioReporter;

/**
 * @author Eric Müller <mekose@protonmail.com>
 * @date 23.01.2015 06:51:11
 * @file ScenarioTest.java
 */
public class ScenarioTest {

	// work/ScenarioTest-Result.txt or work/ScenarioTest-Result.xml
	@Rule
	public IScenarioReporter reporter = Scenarios.reporter("work/ScenarioTest-Result.txt");

	@Test
	public void testAdding() throws Exception {

		final Scenario s1 = Scenarios.newScenario("Test adding to integers");

		int num1 = 3;
		int num2 = 7;
		int res = -1;

		s1.given("Two integers " + num1 + " and " + num2);

		s1.when("Adding them");
		res = num1 + num2;

		s1.then("Result should be 10");
		Assert.assertEquals(10, res);

		// ---

		final Scenario s2 = Scenarios.newScenario("Test adding to other integers");

		num1 = 5;
		num2 = 9;
		res = -1;

		s2.given("Two integers " + num1 + " and " + num2);

		s2.when("Adding them");
		res = num1 + num2;

		s2.then("Result should be 14");
		Assert.assertEquals(14, res);
	}

	@Test
	public void testSubtracting() throws Exception {

		final Scenario s1 = Scenarios.newScenario("Test subtracting to integers");

		int num1 = 3;
		int num2 = 7;
		int res = -1;

		s1.given("Two integers " + num1 + " and " + num2);

		s1.when("Subtracting them");
		res = num1 - num2;

		s1.then("Result should be -4");
		Assert.assertEquals(-4, res);

		// ---

		final Scenario s2 = Scenarios.newScenario("Test subtracting to other integers");

		num1 = 5;
		num2 = 9;
		res = -1;

		s2.given("Two integers " + num1 + " and " + num2);

		s2.when("Subtracting them");
		res = num1 - num2;

		s2.then("Result should be -4");
		Assert.assertEquals(-4, res);
	}
}
